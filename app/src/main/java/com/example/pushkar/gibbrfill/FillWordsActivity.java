package com.example.pushkar.gibbrfill;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;


public class FillWordsActivity extends AppCompatActivity implements View.OnClickListener {

    int current_key = 0;
    ArrayList<HashMap<String,String>> parsed_list;
    EditText editText;
    String file_text;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_words);

        String file_content = read_text_file_from_assets();
//        TextView view = (TextView) findViewById(R.id.set_dynamic_text);
//        view.setText(file_content);

        file_text = file_content;
        parsed_list = parse_file_content(file_content);
        editText = (EditText) findViewById(R.id.edit_text);
        Button button = (Button) findViewById(R.id.submit_button);
        button.setOnClickListener(this);
        capture_words(current_key);

    }

    public void capture_words(int key) {
        TextView textView = (TextView) findViewById(R.id.set_dynamic_text);
        textView.setText((parsed_list.size() - current_key)+" word(s) left");
       HashMap<String,String> hashMap = parsed_list.get(key);
        String hint = hashMap.get("keyword");
        editText.setHint(hint);
        TextView text = (TextView) findViewById(R.id.static_content);
        text.setText("please type a/an "+hint);
    }

    public ArrayList<HashMap<String,String>> parse_file_content(String file_content) {

        ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();
        while(file_content.indexOf("<") >=0) {
            HashMap<String,String> hashMap = new HashMap<>();
            int start_index = file_content.indexOf("<");
            int end_index = file_content.indexOf(">");
            String adj = file_content.substring(start_index+1,end_index);
            hashMap.put("keyword",adj);
            arrayList.add(hashMap);
            file_content = file_content.replaceFirst("<"+adj+">","");
        }
        return arrayList;
    }

    public String read_text_file_from_assets() {
        StringBuilder buf = new StringBuilder();
        InputStream file_content= null;
        try {
            file_content = getAssets().open("gibbr_fill.txt");
            BufferedReader in= null;

            in = new BufferedReader(new InputStreamReader(file_content, "UTF-8"));
            String str;

            while ((str=in.readLine()) != null) {
                buf.append(str);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buf.toString();
    }

    @Override
    public void onClick(View view) {
        if (current_key < parsed_list.size()) {
            String value = editText.getText().toString();

            if (value != null && !value.isEmpty()) {
                if (value.contains("<") || value.contains(">")) {
                    editText.setError("Please Enter Proper Word");
                } else {
                    HashMap<String, String> hashMap = parsed_list.get(current_key);
                    hashMap.put("value", value);
                    current_key++;
                    if(parsed_list.size() == current_key) {
                        current_key = 0;
                        int replace_words = 0;
                        while(file_text.indexOf("<") >=0) {

                            int start_index = file_text.indexOf("<");
                            int end_index = file_text.indexOf(">");
                            String adj = file_text.substring(start_index+1,end_index);
                            HashMap<String, String> word = parsed_list.get(current_key);
                            String replace_value = word.get("value");
                            file_text = file_text.replaceFirst("<"+adj+">",replace_value);
                            current_key++;
                        }
                        while (replace_words < parsed_list.size()) {
                            HashMap<String, String> map = parsed_list.get(replace_words);
                            String replace_word = map.get("value");
                            file_text = file_text.replaceFirst(replace_word,"<b>"+replace_word+"</b>");
                            replace_words++;
                        }
                        Intent intent = new Intent(this,ShowStory.class);
                        intent.putExtra("full_story",file_text);
                        startActivity(intent);
                    } else {
                        editText.getText().clear();
                        capture_words(current_key);
                    }
                }
            } else {
                editText.setError("Please Enter Proper Word");
            }
        } else {
            Log.e("DEBUG","ArrayList size exceeded");
        }
    }
}
