package com.example.pushkar.gibbrfill;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.TextView;

public class ShowStory extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_full_story);
        String show_story = null;
        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle.containsKey("full_story")) {
                show_story = bundle.getString("full_story");
            }
        }

        if (show_story != null) {
        TextView textView = (TextView) findViewById(R.id.story);
        textView.setText(Html.fromHtml(show_story));
        }
    }
}
